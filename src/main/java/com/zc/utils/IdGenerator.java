package com.zc.utils;

/**
 * @author zhaochang.
 * @Date 2022/9/24.
 * @desc
 */
public interface IdGenerator {
    /**
     * id生成
     * @return
     */
    String generate();
}
