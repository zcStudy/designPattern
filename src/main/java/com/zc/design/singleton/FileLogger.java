package com.zc.design.singleton;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * @author zhaochang.
 * @Date 2022/9/24.
 * @desc 饿汉式
 */
@Slf4j
public class FileLogger {
    private FileWriter fileWriter;
    private FileLogger() {
        File file = new File("/Users/zhaochang/log.txt");
        try {
            fileWriter = new FileWriter(file, true);
        } catch (IOException e) {
            log.error("FileLogger 出现io异常", e);
        }
    }
    private static final FileLogger instance = new FileLogger();

    public static FileLogger getInstance() {
        return instance;
    }

    public void log(String message){
        try {
            fileWriter.write(message);
        } catch (IOException e) {
            log.error("写日志出现io异常", e);
        }
    }
}
