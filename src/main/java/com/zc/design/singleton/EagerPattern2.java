package com.zc.design.singleton;

/**
 * @author zhaochang.
 * @Date 2022/9/24.
 * @desc 静态内部类（饿汉式+延迟加载）
 */
public class EagerPattern2 {
    private EagerPattern2(){}
    private static volatile EagerPattern2 instance;

    private static class EagerPattern2Holder{
        private static final EagerPattern2 instance = new EagerPattern2();
    }
    public static EagerPattern2 getInstance() {
        return EagerPattern2Holder.instance;
    }
}
