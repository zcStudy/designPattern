package com.zc.design.singleton;

/**
 * @author zhaochang.
 * @Date 2022/9/24.
 * @desc 直接使用枚举
 */
public enum EnumPattern {
    INSTANCE;
    public void enumMethod(){
    }
}
