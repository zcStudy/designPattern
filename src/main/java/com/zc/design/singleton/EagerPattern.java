package com.zc.design.singleton;

/**
 * @author zhaochang.
 * @Date 2022/9/24.
 * @desc
 */
public class EagerPattern {
    private EagerPattern(){}
    private static final  EagerPattern instance = new EagerPattern();
    public EagerPattern getInstance(){
        return instance;
    }
}
