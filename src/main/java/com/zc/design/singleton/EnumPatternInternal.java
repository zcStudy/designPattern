package com.zc.design.singleton;

/**
 * @author zhaochang.
 * @Date 2022/9/24.
 * @desc 定义内部枚举类通过枚举对象调用方法获取需要的对象
 */
public class EnumPatternInternal {
    private EnumPatternInternal(){}
    public static enum EnumPattern{
        INSTANCE;
        private EnumPatternInternal instance = null;
        private EnumPattern(){
            instance = new EnumPatternInternal();
        }
        public EnumPatternInternal getInstance(){
            return instance;
        }
    }

    public static void main(String[] args) {
        EnumPatternInternal instance = EnumPattern.INSTANCE.getInstance();
    }
}
