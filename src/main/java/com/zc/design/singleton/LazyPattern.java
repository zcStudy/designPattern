package com.zc.design.singleton;

/**
 * @author zhaochang.
 * @Date 2022/9/24.
 * @desc 方法锁
 */
public class LazyPattern {
    private LazyPattern(){}
    private static LazyPattern instance;

    public static synchronized LazyPattern getInstance() {
        if (null == instance){
            instance = new LazyPattern();
        }
        return instance;
    }
}
