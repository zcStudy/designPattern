package com.zc.design.singleton;

/**
 * @author zhaochang.
 * @Date 2022/9/24.
 * @desc 类锁+双重检验
 */
public class LazyPattern2 {
    private LazyPattern2(){}
    private static volatile LazyPattern2 instance;

    public static LazyPattern2 getInstance() {
        if (null == instance){
            synchronized (LazyPattern2.class){
                if (null == instance){
                    instance = new LazyPattern2();
                }
            }
        }

        return instance;
    }
}
