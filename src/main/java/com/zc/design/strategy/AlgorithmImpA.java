package com.zc.design.strategy;

/**
 * @author zhaochang.
 * @Date 2022/9/14.
 * @desc
 */
public class AlgorithmImpA implements Strategy {
    @Override
    public void doAlgorithm() {
        int type = (int) (100 * Math.random());
        if (type == 1) {

        } else if (type == 2) {

        } else if (type == 3) {

        } else {

        }
    }
}
