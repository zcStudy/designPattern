package com.zc.design.strategy;

/**
 * @author zhaochang.
 * @Date 2022/9/14.
 * @desc
 */

public interface Strategy {
    /**
     * 需要执行的算法
     */
    void doAlgorithm();
}
