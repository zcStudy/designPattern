package com.zc;

import java.util.logging.Level;

/**
 * @author zhaochang.
 * @Date 2022/8/29.
 * @desc
 */
public class MessageQueueLogger extends Logger{
    public MessageQueueLogger(String name, boolean enabled, Level minPermittedLevel) {
        super(name, enabled, minPermittedLevel);
    }
    public void log(Level level, String mesage) { if (!isLoggable()) return;
// 格式化 level 和 message, 输出到消息中间件 msgQueueClient.send(...);
    }
}
