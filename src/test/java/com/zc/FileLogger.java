package com.zc;

import java.io.IOException;
import java.io.Writer;
import java.util.logging.Level;

/**
 * @author zhaochang.
 * @Date 2022/8/29.
 * @desc
 */
public class FileLogger extends Logger{
    private Writer fileWriter;

    public FileLogger(String name, boolean enabled, Level minPermittedLevel) {
        super(name, enabled, minPermittedLevel);
    }

    public void log(Level level, String mesage) throws IOException {
        if (!isLoggable()) return;
        fileWriter.write(mesage);
    }
}
