package com.zc;

import java.util.logging.Level;

/**
 * @author zhaochang.
 * @Date 2022/8/29.
 * @desc
 */
public class Logger {
    private String name;
    private boolean enabled;
    private Level minPermittedLevel;

    public Logger(String name, boolean enabled, Level minPermittedLevel) {
        this.name = name;
        this.enabled = enabled;
        this.minPermittedLevel = minPermittedLevel;
    }

    protected boolean isLoggable(){
        boolean loggable = enabled && minPermittedLevel.intValue() > 0;
        return loggable;
    }
}
